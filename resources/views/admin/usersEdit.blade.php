@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">Edit de Usuários</div>

                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                @foreach($users as $user)
                                <form action="{{ url('/home/usersEditSave/'.$user->id) }}" method="POST">
                                    <div class="form-row">
                                        <div class="col-md-12 col-sm-12 form-group">
                                            @csrf
                                            <label>Nome</label>
                                            <input type="text" name="name" class="form-control" autofocus value="{{ $user->name }}" />
                                        </div>
                                        <div class="col-md-12 col-sm-12 form-group">
                                            <label>Email</label>
                                            <input type="text" name="email" class="form-control" autofocus value="{{ $user->email }}" />
                                        </div>
                                        <div class="col-md-12 col-sm-12 form-group">
                                            <label>Nivel</label>
                                            <select name="id_nivel" class="form-control">
                                                <option value selected="true" disabled="true">Selecione o nivel de acesso</option>
                                                @foreach($nivels as $nivel)
                                                    <option value="{{ $nivel->id }}" {{ $user->id_nivel==$nivel->id?'selected':'' }}>{{ $nivel->desc }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12 col-sm-12 form-group">
                                            <button type="submit" class="btn btn-primary">Edit</button>
                                        </div>
                                    </div>
                                </form>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
