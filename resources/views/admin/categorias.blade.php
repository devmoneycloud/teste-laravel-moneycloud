@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">Lista de Categorias</div>

                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>categoria</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>                                    
                                            <td>
                                                <!--<a href="" class="btn btn-primary">Edit</a>
                                                <a href="" class="btn btn-danger" onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">Del</a>
                                                <form id="delete-form" action="" method="POST" style="display: none;">
                                                    @csrf
                                                </form>-->
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>categoria</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
