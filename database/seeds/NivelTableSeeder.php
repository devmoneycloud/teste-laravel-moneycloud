<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NivelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return DB::table('nivels')->insert([
            [
                'nivel' => 'A',
                'desc' => 'Administrador',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nivel' => 'C',
                'desc' => 'Comum',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
