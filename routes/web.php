<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => '/home'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/users', 'HomeController@users')->name('users');
    Route::get('/{id}/usersEdit', 'HomeController@usersEdit')->name('usersEdit');
    Route::post('/usersEditSave/{id}', 'HomeController@usersEditSave')->name('usersEditSave');
    Route::post('/usersDelete/{id}', 'HomeController@usersDelete')->name('usersDelete');
    Route::post('/usersSave', 'HomeController@usersSave')->name('SaveUsers');
    Route::get('/produtos', 'HomeController@produtos')->name('produtos');
    Route::get('/marcas', 'HomeController@marcas')->name('marcas');
    Route::get('/categorias', 'HomeController@categorias')->name('categorias');
});
