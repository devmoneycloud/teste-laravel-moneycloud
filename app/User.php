<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','id_nivel'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function UsersAll()
    {
        return DB::table('users')
                  ->join('nivels', 'users.id_nivel','=','nivels.id')
                  ->select('*')
                  ->get();
    }

    public function UsersFind($id)
    {
        return DB::table('users')
                  ->where('users.id',$id)
                  ->join('nivels', 'users.id_nivel','=','nivels.id')
                  ->select('*')
                  ->get();
    }

    public function usersDelete($id)
    {
        return DB::table('users')
                ->where('id','=',$id)
                ->delete();
    }
}
