<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Nivel;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function users()
    {
        $user = new User();
        $users = $user->UsersAll();
        return view('admin.users',compact('users'));
    }

    public function usersEdit($id)
    {
        $user = new User();
        $users = $user->UsersFind($id);
        $nivel = new Nivel();
        $nivels = $nivel->NivelAll();
        return view('admin.usersEdit',compact('users','nivels'));
    }

    public function usersEditSave(Request $request)
    {
        $user = new User();
        $user->usersEditSave($request->name,$request->email,$request->id_nivel,$request->id);
        return redirect()->to('/home/users');
    }

    public function usersDelete(Request $request)
    {
        $user = new User();
        $user->usersDelete($request->id);
        return redirect()->to('/home/users');
    }

    public function produtos()
    {
        return view('admin.produtos');
    }

    public function categorias()
    {
        return view('admin.categorias');
    }

    public function marcas()
    {
        return view('admin.marcas');
    }
}
