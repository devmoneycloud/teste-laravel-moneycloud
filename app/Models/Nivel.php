<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Nivel extends Model
{
    public function NivelAll()
    {
        return DB::table('nivels')->select('*')->get();
    }
}
